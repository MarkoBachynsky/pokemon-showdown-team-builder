package gui;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.ItemSelectable;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.EventObject;
import java.util.Hashtable;
import java.util.Scanner;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.MatteBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.text.DefaultCaret;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import sun.audio.ContinuousAudioDataStream;
public class PokemonGenerator {
	public static ArrayList<Pokemon> PokemonList = new ArrayList<Pokemon>(); // Is 834
	public static String directory = System.getProperty("user.dir");
	public static ArrayList<ArrayList<ArrayList<String>>> TypeTable;
	public static String[] formats = {"AG", "Uber", "OU", "BL", "UU", "BL2", "RU", "BL3", "NU", "BL4", "PU", "NFE", "LC Uber", "LC" };
	public static Color Bug = new Color(136,150,14), Dark = new Color(60,45,35), Dragon = new Color(78,59,164),
						Electric = new Color(231,147,2), Fairy = new Color(224,142,224), Fighting = new Color(224,142,224),
						Fire = new Color(199,33,0), Flying = new Color(93,115,212), Ghost = new Color(69,69,147),
						Grass = new Color(56,154,2), Ground = new Color(173,140,51), Ice = new Color(109,211,245),
						Normal = new Color(173,165,148), Poison = new Color(107,36,110), Psychic = new Color(220,49,101),
						Rock = new Color(158,134,61), Steel = new Color(142,142,159), Water = new Color(12,103,194);
	
	// Darks old color value (60,45,35)
	public static ArrayList<Pokemon> noBuildList = new ArrayList<Pokemon>();
	public static Updater updateApp = null;
	public static int loadingBarValue = 0, previousTotalBarValue = 834;

	public static void deleteEverything(){
		for(Pokemon s : PokemonList)
			try { Files.deleteIfExists(FileSystems.getDefault().getPath(directory, "Data " + s.getSPECIES().toLowerCase() + ".txt")); } catch (IOException e) { }
	}
	
	public static void downloadData() throws IOException {
		String[] urlList = new String[3];
		URL url;
		InputStream is = null;
		BufferedReader br;
		String line;
		urlList[0] = "https://raw.githubusercontent.com/Zarel/Pokemon-Showdown/master/data/pokedex.js";
		urlList[1] = "https://raw.githubusercontent.com/Zarel/Pokemon-Showdown/master/data/typechart.js";
		urlList[2] = "https://raw.githubusercontent.com/Zarel/Pokemon-Showdown/master/data/formats-data.js";
		for (String link : urlList) {
			RandomAccessFile writer = new RandomAccessFile(new File("Data " + link.substring(link.lastIndexOf("/") + 1, link.length() - 3) + ".txt"), "rw");
			try {
				url = new URL(link);
				is = url.openStream(); // throws an IOException
				br = new BufferedReader(new InputStreamReader(is));
				while ((line = br.readLine()) != null)
					writer.write((line + "\r\n").getBytes());
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (is != null) is.close();
				} catch (IOException ioe) {}
			}
			writer.close();
		}
	}

	public static String[] getDataByText(String name, String regex) throws IOException {
		File file = new File(name);
		String texts = "";
		Scanner sc = new Scanner(file);
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			texts += line;
		}
		sc.close();
		texts = texts.replace("\n", "");
		return texts.split(regex);
	}

	public static String[] getPokemonListNames() {
		String[] name = new String[PokemonList.size()];
		int i = 0;
		for (Pokemon p : PokemonList) {
			name[i] = p.getSPECIES();
			i++;
		}
		return name;
	}

	public static String Replace(String s) {
		return s.toLowerCase().replace("-", "").replace("'", "").replace(".", "").replace(" ", "");
	}

	public static String indexOfSpace(String s) {
		return s.substring(s.indexOf(" ") + 1);
	}

	public static void createPokemonListFile() throws IOException {
		ObjectOutputStream objectOutput = new ObjectOutputStream(new FileOutputStream("PokemonList.bin"));
		if(new File("Data pokedex.txt").exists()){
		Scanner sc = new Scanner(new File("Data pokedex.txt"));
		try {previousTotalBarValue = sc.nextInt(); } catch (Exception e) {}
		sc.close(); }
		String Data = "", Types = "", Type1 = "", Type2 = "", formatName = "";
		String[] data1 = getDataByText("Data pokedex.txt", "_");
		String[] formatData = getDataByText("Data formats-data.txt", "(sets = \\{)|(\t\\},)\t");
		for (String s : data1)
			Data += s;
		String[] numbers = Data.split("num: "), species = Data.split("species: \"");
		String[] types = Data.split("(types: \\[)|(\\],)"), baseStats = Data.split("baseStats: \\{");
		String[] heightm = Data.split("heightm: ");
		String[] weightkg = Data.split("weightkg: ");
		for (int i = 1, k = 0, t = 0; i < numbers.length; i++, k++, t++) {
			while (!(types[k].substring(0, 1).equals("\"")))
				k++;
			int Numbers = Integer.parseInt(numbers[i].substring(0, numbers[i].indexOf(",")));
			String Species = species[i].substring(0, species[i].indexOf("\""));
			String[] subTypes = types[k].split(", ");
			String BaseStat = baseStats[i].substring(0, baseStats[i].indexOf("}"));
			String Heightm = heightm[i].substring(0, heightm[i].indexOf(","));
			String Weightkg = weightkg[i].substring(0, weightkg[i].indexOf(",")), Tier = "";
			try { formatName = formatData[t].substring(0, formatData[t].indexOf(":")); } catch (Exception e) {}
			tierLoop: while (!(formatName.equals(Replace(Species)))) {
				t++;
				try { formatName = formatData[t].substring(0, formatData[t].indexOf(":")); } catch (Exception e) {}
				if (t > formatData.length - 1) {
					t = 0;
					break tierLoop;
				}
			}
			if (formatData[t].contains("tier: ")) Tier = formatData[t].substring(formatData[t].indexOf("tier: ") + 7, formatData[t].lastIndexOf("\""));
			try { Type1 = subTypes[0].substring(1, subTypes[0].lastIndexOf("\"")); } catch (Exception e) {}
			try { Type2 = subTypes[1].substring(1, subTypes[1].lastIndexOf("\"")); } catch (Exception e) {}
			switch (subTypes.length) {
				case 1:
					Types = Type1;
					break;
				case 2:
					Types = Type1 + ", " + Type2;
					break;
			}
			if ((Numbers) > 0) {
				int gen = 0;
				if (Numbers <= 151 && Numbers > 0) // # below or equal to 151
					// and above 0
					gen = 1;
				else if (Numbers <= 251 && Numbers > 151) // # below or equal to
					// 251 and above 151
					gen = 2;
				else if (Numbers <= 386 && Numbers > 251) // # below or equal to
					// 386 and above 251
					gen = 3;
				else if (Numbers <= 493 && Numbers > 386) // # below or equal to
					// 493 and above 386
					gen = 4;
				else if (Numbers <= 649 && Numbers > 493) // # below or equal to
					// 649 and above 493
					gen = 5;
				else if (Numbers <= 721 && Numbers > 649) // # below or equal to
					// 721 and above 649
					gen = 6;
				if (Tier.equals("(OU)")) Tier = "OU";
//				System.out.printf("%20s %10s\n", Species, Tier);
				if (Tier.equals("")) {
					for (int j = PokemonList.size()-1; j > 0; j--) {
						if (Species.contains(PokemonList.get(j).getSPECIES())) {
							Tier = PokemonList.get(j).getTIER();
//							System.out.println(Species + "is now " + Tier + " because of " + PokemonList.get(j).getSPECIES());
							break;
						}
					}
				}
				PokemonList.add(new Pokemon(gen, Numbers, Species, Types, Tier, BaseStat, Float.parseFloat(Heightm), Float.parseFloat(Weightkg)));
				objectOutput.writeObject(PokemonList.get(PokemonList.size() - 1));
				updateApp.TextArea.setText(updateApp.TextArea.getText() + "#" + Numbers + " " + Species + " completed.\n");
				loadingBarValue++;
				double num = (double)loadingBarValue * 100 / previousTotalBarValue;
				String numString = Double.toString(num);
				updateApp.ProgressBar.setValue((int)Math.ceil(num));
				try{updateApp.ProgressBar.setString(numString.substring(0, numString.indexOf(".")+1) + numString.substring(numString.indexOf(".")+1, numString.indexOf(".")+3) + "%");}
				catch (Exception e) {updateApp.ProgressBar.setString(numString);}
//				try { Application.sortNumButtonActionPerformed(new ActionEvent(ActionEvent.ACTION_FIRST, 0, "")); } catch (Exception e) {}
			}
		}
		RandomAccessFile writer = new RandomAccessFile(new File("Data pokedex.txt"), "rw");
		writer.seek(0); // to the beginning
		writer.write((Integer.toString(loadingBarValue)+"\r\n").getBytes());
		writer.close();
		objectOutput.close();
	}
	
	public static void createTypeTable() throws IOException {
		String[] typeChart = getDataByText("Data typechart.txt", "(\\},\t)|( = \\{\t)");
		int i = 0;
		for (String s : typeChart) {
			if (s.startsWith("\"")) {
				s = s.replace("\t", "");
				i++;
			}
		}
		String[] TypeChart = new String[i];
		for (int a = 0, b = 0; a < typeChart.length; a++) {
			if (typeChart[a].startsWith("\"")) {
				TypeChart[b] = typeChart[a].replace("\t", "");
				b++;
			}
		}
		ArrayList<ArrayList<ArrayList<String>>> typeTable = new ArrayList<ArrayList<ArrayList<String>>>(); // Creats the list of types, with 4 cates. , and 18 other types.
		for (i = 0; i < 18; i++) { // 18 Types
			ArrayList<ArrayList<String>> Category = new ArrayList<ArrayList<String>>();
			for (int j = 0; j < 4; j++) { // 4 Categories
				ArrayList<String> toOtherType = new ArrayList<String>();
				for (int k = 0; k < 18; k++) { // 18 Types
				}
				Category.add(toOtherType);
			}
			typeTable.add(Category);
		}
		for (int x = 0; x < TypeChart.length; x++) {
			String belongsTo = TypeChart[x].substring(1, TypeChart[x].indexOf(":") - 1);
			String contents = TypeChart[x].substring(TypeChart[x].indexOf("n:") + 4).replace("\t", "");
			String[] list = contents.split(",");
			for (String type : list) {
				try {
					if (type.startsWith("\"")) {
						if (type.endsWith("0")) { // Normal effect
							String name = type.substring(1, type.lastIndexOf("\""));
							typeTable.get(x).get(0).add(name + " does normal damage to " + belongsTo);
						}
						if (type.endsWith("1")) { // Super effective
							String name = type.substring(1, type.lastIndexOf("\""));
							typeTable.get(x).get(1).add(name + " is double effective to " + belongsTo);
						}
						if (type.endsWith("2")) { // Not effective
							String name = type.substring(1, type.lastIndexOf("\""));
							typeTable.get(x).get(2).add(name + " has half effect on " + belongsTo);
						}
						if (type.endsWith("3")) { // No effect
							String name = type.substring(1, type.lastIndexOf("\""));
							typeTable.get(x).get(3).add(name + " has no effect on " + belongsTo);
						}
					}
				} catch (Exception e) {}
			}
		}
		TypeTable = typeTable;
		ObjectOutputStream objectOutput = new ObjectOutputStream(new FileOutputStream("TypeChart.bin"));
		objectOutput.writeObject(typeTable);
		objectOutput.close();
	}

	@SuppressWarnings("unchecked")
	public static void readTypeTable() {
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("TypeTable.bin"));
			TypeTable = ((ArrayList<ArrayList<ArrayList<String>>>) ois.readObject());
			ois.close();
		} catch (Exception e) {}
	}

	public static void createNoBuildList() throws IOException {
		ObjectOutputStream objectOutput = new ObjectOutputStream(new FileOutputStream("NoBuildList.bin"));
		for (Pokemon p : PokemonList) {
//		try{System.out.println(p.getSPECIES() + "\n" + p.BuildSets.get(0) + " " + p.BuildSets.get(0).size() + " #0");
//			System.out.println(p.BuildSets.get(1) + " " + p.BuildSets.get(1).size() + " #1");
//			System.out.println(p.BuildSets.get(2) + " " + p.BuildSets.get(2).size() + " #2");
//			System.out.println(p.BuildSets.get(3) + " " + p.BuildSets.get(3).size() + " #3");
//			System.out.println(p.BuildSets.get(4) + " " + p.BuildSets.get(4).size() + " #4");
//			System.out.println(p.BuildSets.get(5) + " " + p.BuildSets.get(5).size() + " #5"); } catch (Exception e) {}
			try {
				if (p.BuildSets.get(0).get(0).startsWith("Raises accuracy")) {
					noBuildList.add(p);
//					System.out.println(p.getSPECIES() + " has no builds.");
					objectOutput.writeObject(p);
				}
			} catch (Exception e) {
				try {
					if (p.BuildSets.get(3).size() == 0) {
						noBuildList.add(p);
//						System.out.println(p.getSPECIES() + " has no builds. And hit exception.");
						objectOutput.writeObject(p);
					} } catch (Exception a) {
					noBuildList.add(p);
//					System.out.println(p.getSPECIES() + " has no builds. And hit exception.");
					objectOutput.writeObject(p);
				}
			}
		}
		objectOutput.close();
	}
	
	public static void readNoBuildList() throws IOException {
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream("NoBuildList.bin"));
		while (true)
			try { noBuildList.add((Pokemon) ois.readObject()); } catch (Exception e) {
				break;
			}
//		for(Pokemon p : noBuildList){
			
//			System.out.println("Reading No Build List\t" +p.getSPECIES());
//			if (p.getSPECIES().equals("Unown")) {
//				System.out.println(p.BuildSets.get(0));
//				System.out.println(p.BuildSets.get(1));
//				System.out.println(p.BuildSets.get(2));
//				System.out.println(p.BuildSets.get(3));
//				System.out.println(p.BuildSets.get(4));
//			}
//		}
		ois.close();
	}
	
	public static void readPokemonList() throws IOException {
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream("PokemonList.bin"));
		PokemonList.clear();
		while (true)
			try { PokemonList.add((Pokemon) ois.readObject()); } catch (Exception e) { break; }
		ois.close();
	}

	public static void generateData() throws IOException, ClassNotFoundException { // Checks for files, downloads and creates Data & Pok�mon files
		if (!(new File("Data typechart.txt").exists() && new File("Data pokedex.txt").exists() && new File("Data formats-data.txt").exists())) downloadData();
		if (!(new File("TypeTable.bin").exists())) createTypeTable(); else readTypeTable();
		if (!(new File("PokemonList.bin").exists())) {
			updateApp = new Updater();
			updateApp.setVisible(true);
			createPokemonListFile();
			updateApp.dispose();
		} else readPokemonList();
		if (!(new File("NoBuildList.bin").exists())) createNoBuildList(); else readNoBuildList();
	}

	public static String[][] getSortedPokemonList(String VALUE) {
    	try { PokemonGenerator.readPokemonList(); } catch (IOException e) { }
		if (VALUE.equals("gen")) Collections.sort(PokemonList, Pokemon.Comparators.GEN);
		if (VALUE.equals("num")) Collections.sort(PokemonList, Pokemon.Comparators.NUM);
		if (VALUE.equals("species")) Collections.sort(PokemonList, Pokemon.Comparators.SPECIES);
		if (VALUE.equals("type")) Collections.sort(PokemonList, Pokemon.Comparators.TYPES);
		if (VALUE.equals("tier")) Collections.sort(PokemonList, Pokemon.Comparators.TIER);
		if (VALUE.equals("hp")) Collections.sort(PokemonList, Pokemon.Comparators.HP);
		if (VALUE.equals("atk")) Collections.sort(PokemonList, Pokemon.Comparators.ATK);
		if (VALUE.equals("def")) Collections.sort(PokemonList, Pokemon.Comparators.DEF);
		if (VALUE.equals("spa")) Collections.sort(PokemonList, Pokemon.Comparators.SPA);
		if (VALUE.equals("spd")) Collections.sort(PokemonList, Pokemon.Comparators.SPD);
		if (VALUE.equals("spe")) Collections.sort(PokemonList, Pokemon.Comparators.SPE);
		if (VALUE.equals("totalbase")) Collections.sort(PokemonList, Pokemon.Comparators.TOTALBASE);
		if (VALUE.equals("height")) Collections.sort(PokemonList, Pokemon.Comparators.HEIGHT);
		if (VALUE.equals("weight")) Collections.sort(PokemonList, Pokemon.Comparators.WEIGHT);
		String[][] list = new String[PokemonList.size()][14];
		for (int i = 0; i < PokemonList.size(); i++) {
			list[i][0] = Integer.toString(PokemonList.get(i).getGEN());
			list[i][1] = Integer.toString(PokemonList.get(i).getNUM());
			list[i][2] = PokemonList.get(i).getSPECIES();
			list[i][3] = PokemonList.get(i).getTYPES();
			list[i][4] = PokemonList.get(i).getTIER();
			list[i][5] = Integer.toString(PokemonList.get(i).getHP());
			list[i][6] = Integer.toString(PokemonList.get(i).getATK());
			list[i][7] = Integer.toString(PokemonList.get(i).getDEF());
			list[i][8] = Integer.toString(PokemonList.get(i).getSPA());
			list[i][9] = Integer.toString(PokemonList.get(i).getSPD());
			list[i][10] = Integer.toString(PokemonList.get(i).getSPE());
			list[i][11] = Integer.toString(PokemonList.get(i).getTOTALBASE());
			list[i][12] = Float.toString(PokemonList.get(i).getHEIGHT());
			list[i][13] = Float.toString(PokemonList.get(i).getWEIGHT());
		}
		return list;
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		try {UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel"); } catch (Exception e) {}
//		deleteEverything();
		generateData();
		new Application().setVisible(true);
	}
}
class Pokemon extends PokemonGenerator implements Comparable<Pokemon>, Serializable {
	private static final long serialVersionUID = 1L;
	private int GEN, NUM, HP, ATK, DEF, SPA, SPD, SPE, TOTALBASE;
	private float HEIGHT, WEIGHT;
	private String SPECIES, TIER;
	private String[] TYPES;
	public ArrayList<ArrayList<String>> BuildSets = new ArrayList<ArrayList<String>>();

	public Pokemon(int Generation, int PokeNumber, String Species, String Types, String Tier, String Stats, float Height, float Weight) {
		GEN = Generation;
		NUM = PokeNumber;
		SPECIES = Species;
		fixNames();
		TYPES = Types.split(", ");
		TIER = Tier;
		String[] stats = Stats.split(", ");
		HP = Integer.parseInt(indexOfSpace(stats[0]));
		ATK = Integer.parseInt(indexOfSpace(stats[1]));
		DEF = Integer.parseInt(indexOfSpace(stats[2]));
		SPA = Integer.parseInt(indexOfSpace(stats[3]));
		SPD = Integer.parseInt(indexOfSpace(stats[4]));
		SPE = Integer.parseInt(indexOfSpace(stats[5]));
		HEIGHT = Height;
		WEIGHT = Weight;
		boolean mayBuild = true;
		Species = Species.toLowerCase();
		ArrayList<String> badNames = new ArrayList<String>();
		badNames.add("mega");
		badNames.add("primal");
		badNames.add("zen");
		badNames.add("blade");
		badNames.add("pirouette");
		for (String s : badNames) {
			if (Species.contains(s)) { // if the name contains an element that will redirect you to the correct webpage don't make a build
				mayBuild = false;
				break;
			}
		}
		if(mayBuild == true){
			try {
				downloadData("http://www.smogon.com/dex/xy/pokemon/" + Species + "/");
				createBuild(); } catch (Exception e) {
//					System.out.println(SPECIES + " could not get a build");
					}}
		TOTALBASE = HP + ATK + DEF + SPA + SPD + SPE;
	}

	public void fixNames(){ // Changing some species names to match smogon links
			if(getSPECIES().equals("Meowstic"))
				setSPECIES("Meowstic-m");
	}
	
	public static void downloadData(String link) throws IOException {
		URL url;
		InputStream is = null;
		BufferedReader br;
		String line;
		RandomAccessFile writer = new RandomAccessFile(new File("Data " + link.substring(link.indexOf("pokemon") + 8, link.length() - 1) + ".txt"), "rw");
		try {
			url = new URL(link);
			is = url.openStream(); // throws an IOException
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null)
				writer.write((line + "\r\n").getBytes());
		} catch (Exception e) {} finally {
			try {
				if (is != null) is.close();
			} catch (IOException ioe) {}
		}
		writer.close();
	}

	public static String[] getDataByText(String name, String regex) throws IOException {
		File file = new File(name);
		String texts = "";
		Scanner sc = new Scanner(file);
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			texts += line;
		}
		sc.close();
		return texts.split(regex);
	}

	public void createBuild() throws IOException {
		String[] strings = getDataByText("Data " + this.getSPECIES() + ".txt", "(<)|(\\>\n)|(,\"description\":\"<h1)");
		for (int i = 0; i < 6; i++) {
			ArrayList<String> Category = new ArrayList<String>(); // Name, item, abiltity, moves, ev, nature 
			BuildSets.add(Category);
		}
		for (String a : strings) {
			try {
				if (!a.substring(a.lastIndexOf(":\"")).equals(":\"")) {
					String buildName = a.substring(a.lastIndexOf(":\"") + 2, a.length() - 1);
					BuildSets.get(0).add(buildName);
				}
			} catch (Exception e) {}
			if (a.contains("abilities\"") && !a.contains("dex")) {
				String abilities = "", items = "", moveslots = "", ev = "", nature = "";
				if (!(a.substring(a.indexOf("["), a.indexOf("[") + 2).equals("[]"))) // Parses abilities
					abilities = a.substring(a.indexOf("[") + 1, a.indexOf("]")).replace("\"", "");
				if (!(a.substring(a.indexOf("items") + 7, a.indexOf("items") + 9).equals("[]"))) // Parses items
					items = a.substring(a.indexOf("items") + 8, a.indexOf("moveslots") - 3).replace("\"", "");
				if (!(a.substring(a.indexOf("moveslots") + 11, a.indexOf("moveslots") + 13).equals("[]"))) // Parses moves
					moveslots = a.substring(a.indexOf("moveslots") + 11, a.indexOf("evconfigs") - 3).replace("\"", "").replace("[", "").replace("]", "");
				if (!(a.substring(a.indexOf("evconfigs") + 11, a.indexOf("evconfigs") + 13).equals("[]"))) // Parses ev's
					ev = a.substring(a.indexOf("evconfigs") + 13, a.indexOf("ivconfigs") - 4).replace("\"", "");
				String natureString = a.substring(a.indexOf("natures") + 9);
				if (!(a.substring(a.indexOf("natures") + 9, a.indexOf("natures") + 11).equals("[]"))) // Parses nature
					nature = natureString.substring(natureString.indexOf("\"") + 1, natureString.indexOf("]") - 1).replace("\"", "");
				BuildSets.get(1).add(items);
				BuildSets.get(2).add(abilities);
				BuildSets.get(3).add(moveslots);
				String[] evList = ev.split(",");
				ev = "EVs:";
				for (int i = 0; i < evList.length; i++) {
					String letters = evList[i].substring(0, evList[i].indexOf(":"));
					String numbers = evList[i].substring(evList[i].indexOf(":") + 1, evList[i].length());
					if (letters.equals("hp")) letters = "HP";
					if (letters.equals("atk")) letters = "Atk";
					if (letters.equals("def")) letters = "Def";
					if (letters.equals("spa")) letters = "SpA";
					if (letters.equals("spd")) letters = "SpD";
					if (letters.equals("spe")) letters = "Spe";
					if (!numbers.equals("0")) ev += " " + numbers + " " + letters + " /";
				}
				ev = ev.substring(0, ev.length() - 2);
				BuildSets.get(4).add(ev);
				BuildSets.get(5).add(nature);
			}
		}
		Files.deleteIfExists(FileSystems.getDefault().getPath(directory, "Data " + this.getSPECIES().toLowerCase() + ".txt"));
		//		for(int i = 0; i < BuildSets.size()-1; i++){
		//			System.out.println("Build Name: " + BuildSets.get(0).get(i));
		//			System.out.println("Item: " + BuildSets.get(1).get(i));
		//			System.out.println("Abilities: " + BuildSets.get(2).get(i));
		//			System.out.println("Moves: " + BuildSets.get(3).get(i));
		//			System.out.println("Evs: " + BuildSets.get(4).get(i));
		//			System.out.println("Nature: " + BuildSets.get(5).get(i));
		//			System.out.println("\n\n");
		//		}
	}

	@Override
	public int compareTo(Pokemon otherPokemon) {
		return this.getSPECIES().compareTo(otherPokemon.getSPECIES()); // Ascending
	}

	public int getTOTALBASE() {
		return TOTALBASE;
	}

	@Override
	public String toString() {
		return "Gen " + getGEN() + " | #" + getNUM() + " | Species " + getSPECIES() + " | Types " + getTYPES() + " | Tier " + getTIER() + " | HP " + getHP() + ", ATK " + getATK() + ", DEF " + getDEF() + ", SPA " + getSPA() + ", SPD " + getSPD() + ", SPE " + getSPE() + ", TOTALBASE " + getTOTALBASE()
				+ " | Height " + getHEIGHT() + " | Weight " + getWEIGHT();
	}

	public int getGEN() {
		return GEN;
	}

	public int getNUM() {
		return NUM;
	}

	public int getHP() {
		return HP;
	}

	public int getATK() {
		return ATK;
	}

	public int getDEF() {
		return DEF;
	}

	public int getSPA() {
		return SPA;
	}

	public int getSPD() {
		return SPD;
	}

	public int getSPE() {
		return SPE;
	}

	public float getHEIGHT() {
		return HEIGHT;
	}

	public float getWEIGHT() {
		return WEIGHT;
	}

	public String getSPECIES() {
		return SPECIES;
	}

	public String getTYPES() {
		return Arrays.toString(TYPES).substring(1, Arrays.toString(TYPES).length() - 1);
	}

	public String getTIER() {
		return TIER;
	}
	public static class Comparators {
		public static Comparator<Pokemon> GEN = new Comparator<Pokemon>() { // Sort
			@Override
			public int compare(Pokemon o1, Pokemon o2) {
				int i = o1.getGEN() - o2.getGEN();
				if (i == 0) i = o1.getSPECIES().compareTo(o2.getSPECIES());
				return i;
			}
		};
		public static Comparator<Pokemon> NUM = new Comparator<Pokemon>() { // Sort
			@Override
			public int compare(Pokemon o1, Pokemon o2) {
				int i = o1.getNUM() - o2.getNUM();
				if (i == 0) i = o1.getSPECIES().compareTo(o2.getSPECIES());
				return i;
			}
		};
		public static Comparator<Pokemon> SPECIES = new Comparator<Pokemon>() { // Sort
			@Override
			public int compare(Pokemon o1, Pokemon o2) {
				return o1.getSPECIES().compareTo(o2.getSPECIES());
			}
		};
		public static Comparator<Pokemon> TYPES = new Comparator<Pokemon>() { // Sorts
			@Override
			public int compare(Pokemon o1, Pokemon o2) {
				int i = o1.getTYPES().compareTo(o2.getTYPES());
				if (i == 0) i = o1.getSPECIES().compareTo(o2.getSPECIES());
				return i;
			}
		};
		public static Comparator<Pokemon> TIER = new Comparator<Pokemon>() { // Sorts
			@Override
			public int compare(Pokemon o1, Pokemon o2) {
				int i = o1.getTIER().compareTo(o2.getTIER());
				if (i == 0) i = o1.getSPECIES().compareTo(o2.getSPECIES());
				return i;
			}
		};
		public static Comparator<Pokemon> HP = new Comparator<Pokemon>() { // Sorts
			@Override
			public int compare(Pokemon o1, Pokemon o2) {
				int i = o2.getHP() - o1.getHP();
				if (i == 0) i = o1.getSPECIES().compareTo(o2.getSPECIES());
				return i;
			}
		};
		public static Comparator<Pokemon> ATK = new Comparator<Pokemon>() { // Sorts
			@Override
			public int compare(Pokemon o1, Pokemon o2) {
				int i = o2.getATK() - o1.getATK();
				if (i == 0) i = o1.getSPECIES().compareTo(o2.getSPECIES());
				return i;
			}
		};
		public static Comparator<Pokemon> DEF = new Comparator<Pokemon>() { // Sorts
			@Override
			public int compare(Pokemon o1, Pokemon o2) {
				int i = o2.getDEF() - o1.getDEF();
				if (i == 0) i = o1.getSPECIES().compareTo(o2.getSPECIES());
				return i;
			}
		};
		public static Comparator<Pokemon> SPA = new Comparator<Pokemon>() { // Sorts
			public int compare(Pokemon o1, Pokemon o2) {
				int i = o2.getSPA() - o1.getSPA();
				if (i == 0) i = o1.getSPECIES().compareTo(o2.getSPECIES());
				return i;
			}
		};
		public static Comparator<Pokemon> SPD = new Comparator<Pokemon>() { // Sorts
			@Override
			public int compare(Pokemon o1, Pokemon o2) {
				int i = o2.getSPD() - o1.getSPD();
				if (i == 0) i = o1.getSPECIES().compareTo(o2.getSPECIES());
				return i;
			}
		};
		public static Comparator<Pokemon> SPE = new Comparator<Pokemon>() { // Sorts
			@Override
			public int compare(Pokemon o1, Pokemon o2) {
				int i = o2.getSPE() - o1.getSPE();
				if (i == 0) i = o1.getSPECIES().compareTo(o2.getSPECIES());
				return i;
			}
		};
		public static Comparator<Pokemon> TOTALBASE = new Comparator<Pokemon>() { // Sorts
			@Override
			public int compare(Pokemon o1, Pokemon o2) {
				int i = o2.getTOTALBASE() - o1.getTOTALBASE();
				if (i == 0) i = o1.getSPECIES().compareTo(o2.getSPECIES());
				return i;
			}
		};
		public static Comparator<Pokemon> HEIGHT = new Comparator<Pokemon>() { // Sorts
			@Override
			public int compare(Pokemon o1, Pokemon o2) {
				int i = o2.getHEIGHT() > o1.getHEIGHT() ? 1 : o2.getHEIGHT() < o1.getHEIGHT() ? -1 : 0;
				if (i == 0) i = o1.getSPECIES().compareTo(o2.getSPECIES());
				return i;
			}
		};
		public static Comparator<Pokemon> WEIGHT = new Comparator<Pokemon>() { // Sorts
			@Override
			public int compare(Pokemon o1, Pokemon o2) {
				int i = o2.getWEIGHT() > o1.getWEIGHT() ? 1 : o2.getWEIGHT() < o1.getWEIGHT() ? -1 : 0;
				if (i == 0) i = o1.getSPECIES().compareTo(o2.getSPECIES());
				return i;
			}
		};
	}
	public void setSPECIES(String species) {
		SPECIES = species;
	}
}
/**
 * @author Marko
 */
class Updater extends javax.swing.JFrame {
	private static final long serialVersionUID = 1L;

	public Updater() {
		initComponents();
	}

	private void initComponents() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("pgIcon.png"));
		Panel = new javax.swing.JPanel();
		ScrollPane = new javax.swing.JScrollPane();
		Label = new javax.swing.JLabel();
		ProgressBar = new javax.swing.JProgressBar();
		ProgressBar.setForeground(Color.green);
		ProgressBar.setStringPainted(true);
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setTitle("PG Updater");
		setPreferredSize(new java.awt.Dimension(300, 500));
		setResizable(false);
		DefaultCaret caret = (DefaultCaret) TextArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		TextArea.setColumns(20);
		TextArea.setRows(5);
		TextArea.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
		TextArea.setFocusable(false);
		TextArea.setMaximumSize(new java.awt.Dimension(164, 94));
		ScrollPane.setViewportView(TextArea);
		Label.setFont(new java.awt.Font("Yu Mincho Light", 0, 36));
		Label.setText("Updater");
		javax.swing.GroupLayout PanelLayout = new javax.swing.GroupLayout(Panel);
		Panel.setLayout(PanelLayout);
		PanelLayout.setHorizontalGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(PanelLayout.createSequentialGroup().addContainerGap()
						.addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(ScrollPane)
								.addGroup(PanelLayout.createSequentialGroup().addGap(52, 52, 52).addComponent(ProgressBar, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE).addGap(62, 62, 62)))
						.addContainerGap())
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelLayout.createSequentialGroup().addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addComponent(Label).addGap(90, 90, 90)));
		PanelLayout.setVerticalGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelLayout.createSequentialGroup().addComponent(Label).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
				.addComponent(ProgressBar, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE).addGap(23, 23, 23).addComponent(ScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap()));
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addContainerGap()));
		pack();
	}

	public static void main(String args[]) {
		try {
			UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HifiLookAndFeel"); 
			} catch (Exception e) { }

	}
	// Variables declaration - do not modify                     
	private javax.swing.JLabel Label;
	private javax.swing.JPanel Panel;
	public javax.swing.JProgressBar ProgressBar;
	public javax.swing.JTextArea TextArea = new javax.swing.JTextArea();
	private javax.swing.JScrollPane ScrollPane;
	// End of variables declaration                   
}
/**
 * @author Marko
 */

@SuppressWarnings("serial")
class MyTableModel extends AbstractTableModel {
	Object[][] data = { 
			{ "", "", "", "", "", "", "", "", "" },
			{ "", "", "", "", "", "", "", "", "" },
			{ "", "", "", "", "", "", "", "", "" },
			{ "", "", "", "", "", "", "", "", "" },
			{ "", "", "", "", "", "", "", "", "" },
			{ "", "", "", "", "", "", "", "", "" },
			};
	
	String[] columnNames = { "Build Name", "Pok�mon", "Type", 
		"Ability", "Move 1", "Move 2", "Move 3",
		"Move 4", "Item" };
	
	public final Object[] longValues = {null, null, null, null, null, null, null, null, null};

	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return data.length;
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	public boolean isCellEditable(int row, int col) {
		if (col != 0) {
			return false;
		} else {
			return true;
		}
	}

	public void setValueAt(Object value, int row, int col) {
		data[row][col] = value;
		fireTableCellUpdated(row, col);
	}

}


class EachRowEditor implements TableCellEditor {
	protected Hashtable editors;
	protected TableCellEditor editor, defaultEditor;
	JTable table;

	public EachRowEditor(JTable table) {
		this.table = table;
		editors = new Hashtable();
		defaultEditor = new DefaultCellEditor(new JTextField());
	}

	public void setEditorAt(int row, TableCellEditor editor) {
		editors.put(new Integer(row), editor);
	}

	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		//editor = (TableCellEditor)editors.get(new Integer(row));
		//if (editor == null) {
		//  editor = defaultEditor;
		//}
		return editor.getTableCellEditorComponent(table, value, isSelected, row, column);
	}

	public Object getCellEditorValue() {
		return editor.getCellEditorValue();
	}

	public boolean stopCellEditing() {
		return editor.stopCellEditing();
	}

	public void cancelCellEditing() {
		editor.cancelCellEditing();
	}

	public boolean isCellEditable(EventObject anEvent) {
		selectEditor((MouseEvent) anEvent);
		return editor.isCellEditable(anEvent);
	}

	public void addCellEditorListener(CellEditorListener l) {
		editor.addCellEditorListener(l);
	}

	public void removeCellEditorListener(CellEditorListener l) {
		editor.removeCellEditorListener(l);
	}

	public boolean shouldSelectCell(EventObject anEvent) {
		selectEditor((MouseEvent) anEvent);
		return editor.shouldSelectCell(anEvent);
	}

	protected void selectEditor(MouseEvent e) {
		int row;
		if (e == null) {
			row = table.getSelectionModel().getAnchorSelectionIndex();
		} else {
			row = table.rowAtPoint(e.getPoint());
		}
		editor = (TableCellEditor) editors.get(new Integer(row));
		if (editor == null) {
			editor = defaultEditor;
		}
	}
}

class Application extends javax.swing.JFrame{
	private static final long serialVersionUID = 1L;
	public ArrayList<ArrayList<Pokemon>> teams = new ArrayList<ArrayList<Pokemon>>();
	public ArrayList<Pokemon> selectedList = new ArrayList<Pokemon>();
	public int pokeNum = 0; // Used in generate team method
	@SuppressWarnings("restriction")
	AudioPlayer MGP = AudioPlayer.player;
	AudioStream BGM; int comboNum = 0;
	ContinuousAudioDataStream loop = null;
	public boolean music = false;
	public Application() {
		
		initComponents();
//		setBackground();
	}
	
	
	public void setBuildSetsColumn(JTable table, TableColumn buildColumn) {
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.addItem("");
		DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
		buildColumn.setCellEditor(new DefaultCellEditor(comboBox));
		renderer.setToolTipText("Click for combo box");
		buildColumn.setCellRenderer(renderer);
	}

	public static String selectedString(ItemSelectable is) {
	    Object selected[] = is.getSelectedObjects();
	    return ((selected.length == 0) ? "null" : (String)selected[0]);
	}
	
	public boolean equalLists(ArrayList<String> one, ArrayList<String> two) {
		if (one == null && two == null) { return true; }
		if ((one == null && two != null) || one != null && two == null || one.size() != two.size()) { return false; }
		one = new ArrayList<String>(one);
		two = new ArrayList<String>(two);
		Collections.sort(one);
		Collections.sort(two);
		return one.equals(two);
	}
	
	public void changeBuildSet(JTable table) {
		JComboBox<String> comboBox = new JComboBox<String>();
		ActionListener actionListener; 
		EachRowEditor rowEditor = new EachRowEditor(table);
		ArrayList<ArrayList<String>> arrayList = new ArrayList<ArrayList<String>>();
		arrayList.add(teams.get(index).get(0).BuildSets.get(0));
		arrayList.add(teams.get(index).get(1).BuildSets.get(0));
		arrayList.add(teams.get(index).get(2).BuildSets.get(0));
		arrayList.add(teams.get(index).get(3).BuildSets.get(0));
		arrayList.add(teams.get(index).get(4).BuildSets.get(0));
		arrayList.add(teams.get(index).get(5).BuildSets.get(0));
		for (int i = 0; i < arrayList.size(); i++) {
			for (int k = 0; k < arrayList.get(i).size(); k++)
				comboBox.addItem(arrayList.get(i).get(k));
			rowEditor.setEditorAt(i, new DefaultCellEditor(comboBox));
			actionListener = new ActionListener() {
				public void actionPerformed(ActionEvent actionEvent) {
					int row = 0;
	                @SuppressWarnings("unchecked")
					JComboBox<String> comboBox1 = (JComboBox<String>) actionEvent.getSource();
	                ArrayList<String> contents = new ArrayList<String>();
	                ArrayList<String> otherContents = new ArrayList<String>();
					String build = (String) comboBox1.getSelectedItem();
					int buildNumber = 0;
	                for(int i1 = 0; i1 < comboBox1.getItemCount(); i1++)
	                	contents.add(comboBox1.getItemAt(i1));
	                for(int j = 0; j < 6; j++){ // Need row number
	                	for(int k = 0; k < teams.get(index).get(j).BuildSets.get(0).size(); k++){
	                		if(teams.get(index).get(j).BuildSets.get(0).get(k).equals(build)) buildNumber = k;
	                		otherContents.add(teams.get(index).get(j).BuildSets.get(0).get(k));
	                	}
	                	if(equalLists(contents, otherContents)){
	                		row = j;
	                		break;
	                	}
	                	otherContents.clear();
	                }
					try {teamTable.setValueAt(teams.get(index).get(row).getSPECIES(), row, 1); } catch (Exception e) { } // Pokemon name
					try {teamTable.setValueAt(teams.get(index).get(row).getTYPES(), row, 2); } catch (Exception e) {} // Type
					
					try {if(teams.get(index).get(row).BuildSets.get(2).get(buildNumber).contains(",")){
						String[] multipleElements = teams.get(index).get(row).BuildSets.get(2).get(buildNumber).split(",");
						try {teamTable.setValueAt(multipleElements[0], row, 3); } catch (Exception e) {}} // Abilities
						else try{teamTable.setValueAt(teams.get(index).get(row).BuildSets.get(2).get(buildNumber), row, 3); } catch (Exception e) {}} catch (Exception e) {}
					
					try {if(teams.get(index).get(row).BuildSets.get(3).get(buildNumber).contains(",")){
					String[] multipleElements = teams.get(index).get(row).BuildSets.get(3).get(buildNumber).split(","); // Moves
						for (int j = 0; j < 4; j++)
							try{teamTable.setValueAt(multipleElements[j], row, (4 + j));} catch (Exception e) {}} // - Moves
					else try{teamTable.setValueAt(teams.get(index).get(row).BuildSets.get(3).get(buildNumber), row, 4); } catch (Exception e) {}} catch (Exception e) {}
						try{if (teams.get(index).get(row).BuildSets.get(1).get(buildNumber).contains(",")) {
							String[] multipleElements = teams.get(index).get(row).BuildSets.get(1).get(buildNumber).split(","); // Items
							teamTable.setValueAt(multipleElements[0], row, 8); // - Items -
						} else teamTable.setValueAt(teams.get(index).get(row).BuildSets.get(1).get(buildNumber), row, 8);  } catch (Exception e) {} // - Items
						updateTextArea();
				
					String type = teams.get(index).get(row).getTYPES();
					Color color = getColorFromTypeCell(type);
					try{teamTable.setValueAt(updateTextWithColor(teams.get(index).get(row).getSPECIES(), color), row, 1); } catch (Exception e) { } // Pokemon name with color
					try{teamTable.setValueAt(updateTextWithColor(teams.get(index).get(row).getTYPES(),color), row, 2); } catch (Exception e) {} // Type with color
					
					try{if(teams.get(index).get(row).BuildSets.get(2).get(0).contains(",")){ // Abilities with color
						String[] multipleElements = teams.get(index).get(row).BuildSets.get(2).get(0).split(","); // Abilities with color
								try{teamTable.setValueAt(updateTextWithColor(multipleElements[0], color), row, 3); // Abilities with color
								teamTable.setValueAt(updateTextWithColor(multipleElements[0], color), row, 3);} catch (Exception e) {}} // Abilities with color
						else try{teamTable.setValueAt(updateTextWithColor(teams.get(index).get(row).BuildSets.get(2).get(0), color), row, 3); } catch (Exception e) {}} catch (Exception e) {}
	
					try{if(teams.get(index).get(row).BuildSets.get(3).get(buildNumber).contains(",")){
						String[] multipleElements = teams.get(index).get(row).BuildSets.get(3).get(buildNumber).split(","); // Moves
							for (int j = 0; j < 4; j++)
								try{teamTable.setValueAt(updateTextWithColor(multipleElements[j], color), row, (4 + j)); // - Moves
								teamTable.setValueAt(updateTextWithColor(multipleElements[j], color), row, (4 + j));} catch (Exception e) {}} // - Moves}
						else try{teamTable.setValueAt(updateTextWithColor(teams.get(index).get(row).BuildSets.get(3).get(buildNumber), color), row, 4); } catch (Exception e) {}} catch (Exception e) {}
							try{if (teams.get(index).get(row).BuildSets.get(1).get(buildNumber).contains(",")) {
								String[] multipleElements = teams.get(index).get(row).BuildSets.get(1).get(buildNumber).split(","); // Items
								teamTable.setValueAt(updateTextWithColor(multipleElements[0], color), row, 8); // - Items -
							} else teamTable.setValueAt(updateTextWithColor(teams.get(index).get(row).BuildSets.get(1).get(buildNumber),color), row, 8);  } catch (Exception e) {} // - Items
		}
			};
			comboBox.addActionListener(actionListener);
			comboBox = new JComboBox<String>();
			try{teamTable.setValueAt(arrayList.get(i).get(0), i, 0);}catch(Exception e){}
			
		}
		table.getColumn("Build Name").setCellEditor(rowEditor);
	}
	
	private void initColumnSizes(JTable table) {
		MyTableModel model = (MyTableModel) table.getModel();
		TableColumn column = null;
		Component comp = null;
		int headerWidth = 0;
		int cellWidth = 0;
		Object[] longValues = model.longValues;
		TableCellRenderer headerRenderer = table.getTableHeader().getDefaultRenderer();
		for (int i = 0; i < 3; i++) {
			column = table.getColumnModel().getColumn(i);
			comp = headerRenderer.getTableCellRendererComponent(table, column.getHeaderValue(), true, true, 0, 0);
			headerWidth = comp.getPreferredSize().width;
			comp = table.getDefaultRenderer(model.getColumnClass(i)).getTableCellRendererComponent(table, longValues[i], true, true, 0, i);
			cellWidth = comp.getPreferredSize().width;
			column.setPreferredWidth(Math.max(headerWidth, cellWidth));
		}
	}
	
	public void setBackground() {
		try {
			setLayout(new BorderLayout());
			File file = new File("poke_2.png");
			JLabel background = new JLabel(new ImageIcon(file.getAbsolutePath()));
			add(background);
			background.setLayout(new FlowLayout());
		} catch (Exception e) {}
	}
    
	public static void resizeColumnWidth(JTable table) {
		TableColumnModel columnModel = table.getColumnModel();
		for (int column = 1; column < table.getColumnCount(); column++) {
			int width = 50; // Min width
			for (int row = 0; row < table.getRowCount(); row++) {
				TableCellRenderer renderer = table.getCellRenderer(row, column);
				Component comp = table.prepareRenderer(renderer, row, column);
				width = Math.max(comp.getPreferredSize().width + 1, width);
			}
			columnModel.getColumn(column).setPreferredWidth(width);
		}
	}

	public static Color createColor(String string){
		Color color = null;
		if(string.equals("Bug")) color = PokemonGenerator.Bug;
		if(string.equals("Dark")) color = PokemonGenerator.Dark;
		if(string.equals("Dragon")) color = PokemonGenerator.Dragon;
		if(string.equals("Electric")) color = PokemonGenerator.Electric;
		if(string.equals("Fairy")) color = PokemonGenerator.Fairy;
		if(string.equals("Fighting")) color = PokemonGenerator.Fighting;
		if(string.equals("Fire")) color = PokemonGenerator.Fire;
		if(string.equals("Flying")) color = PokemonGenerator.Flying;
		if(string.equals("Ghost")) color = PokemonGenerator.Ghost;
		if(string.equals("Grass")) color = PokemonGenerator.Grass;
		if(string.equals("Ground")) color = PokemonGenerator.Ground;
		if(string.equals("Ice")) color = PokemonGenerator.Ice;
		if(string.equals("Normal")) color = PokemonGenerator.Normal;
		if(string.equals("Poison")) color = PokemonGenerator.Poison;
		if(string.equals("Psychic")) color = PokemonGenerator.Psychic;
		if(string.equals("Rock")) color = PokemonGenerator.Rock;
		if(string.equals("Steel")) color = PokemonGenerator.Steel;
		if(string.equals("Water")) color = PokemonGenerator.Water;
		return color;
	}

	public static String colorToRGBString(Color color){
		String result = "rgb(";
		result += color.getRed() + ",";
		result += color.getGreen() + ",";
		result += color.getBlue() + ")";
		return result;
	}
	
	public static Color getColorFromTypeCell(String text){
        if (!text.equals("") && !text.contains(",")) { // If the type cell isn't empty and is single type
            return createColor(text); }
        else if (!text.equals("") && text.contains(",")){ // If the type cell isn't empty and is multi type
        	String[] colors = text.split(", ");
        	String c1 = colors[0];
        	String c2 = colors[1];
        	Color color1 = createColor(c1);
        	Color color2 = createColor(c2);
        	int[] difference = new int[3];
        	int r = 0, g = 0, b = 0;
        	r = color1.getRed()+color2.getRed()/2;
        	if(r > 255){ difference[0] = 255 - r; r = 255;}
        	g = color1.getGreen()+color2.getGreen()/2;
        	if(g > 255){ difference[1] = 255 - g; g = 255;}
        	b = color1.getBlue()+color2.getBlue()/2;
        	if(b > 255){ difference[2] = 255 - b; b = 255;}
        	g -= difference[0]; b -= difference[0];
        	r -= difference[1]; b -= difference[1];
        	r -= difference[2]; g -= difference[2];
        	if(r < 0) r = 0; if(r > 255) r = 255;
        	if(g < 0) g = 0; if(g > 255) g = 255;
        	if(b < 0) b = 0; if(b > 255) b = 255;
        	return new Color(r,g,b);
        }
        return new Color(255,255,255);
	}
	
	public static String updateTextWithColor(String text, Color color){ // Should be split into two methods, will do later
        return  text = "<html><strong><font color="+ colorToRGBString(color) + ">" + text +  "</font></strong></html>";
	}
	
	@SuppressWarnings("serial")
	private void initComponents() {
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("pgIcon.png"));
		teamTextScrollPane = new javax.swing.JScrollPane();
		teamTextArea = new javax.swing.JTextArea();
		pokemonScrollPane = new javax.swing.JScrollPane();
		pokemonTable = new javax.swing.JTable(){
				public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
					Component comp = super.prepareRenderer(renderer, row, col);
					comp.setBackground(new Color(80,80,80));
					return comp;
				}
		};
		teamResultsLabel = new javax.swing.JLabel();
		sortNumButton = new javax.swing.JButton();
		sortTypeButton = new javax.swing.JButton();
		sortSpeciesButton = new javax.swing.JButton();
		sortHpButton = new javax.swing.JButton();
		sortAtkButton = new javax.swing.JButton();
		sortTierButton = new javax.swing.JButton();
		sortSpaButton = new javax.swing.JButton();
		sortDefButton = new javax.swing.JButton();
		sortSpdButton = new javax.swing.JButton();
		sortSpeButton = new javax.swing.JButton();
		sortTotalButton = new javax.swing.JButton();
		sortHeightButton = new javax.swing.JButton();
		sortWeightButton = new javax.swing.JButton();
		upButton = new javax.swing.JButton();
		downButton = new javax.swing.JButton();
		quitButton = new javax.swing.JButton();
		musicButton = new javax.swing.JButton();
		helpButton = new javax.swing.JButton();
		sortByLabel = new javax.swing.JLabel();
		copyTextButton = new javax.swing.JButton();
		updateFilesButton = new javax.swing.JButton();
		genOneCheckBox = new javax.swing.JCheckBox();
		genTwoCheckBox = new javax.swing.JCheckBox();
		genThreeCheckBox = new javax.swing.JCheckBox();
		genFourCheckBox = new javax.swing.JCheckBox();
		genFiveCheckBox = new javax.swing.JCheckBox();
		genSixCheckBox = new javax.swing.JCheckBox();
		doNotIncludeLabel = new javax.swing.JLabel();
		generateTeamButton = new javax.swing.JButton();
		formatComboBox = new javax.swing.JComboBox<>();
		formatLabel = new javax.swing.JLabel();
		doNotIncludeLabelTable = new javax.swing.JLabel();
		genTableSixCheckBox = new javax.swing.JCheckBox();
		genTableFiveCheckBox = new javax.swing.JCheckBox();
		genTableFourCheckBox = new javax.swing.JCheckBox();
		genTableThreeCheckBox = new javax.swing.JCheckBox();
		genTableTwoCheckBox = new javax.swing.JCheckBox();
		genTableOneCheckBox = new javax.swing.JCheckBox();
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setTitle("Pok�mon Team Generator!");
		setBackground(new java.awt.Color(0, 0, 0));
		setMinimumSize(new java.awt.Dimension(1287, 817));
		setResizable(false);
		teamTable = new JTable(new MyTableModel()){
			public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
				Component comp = super.prepareRenderer(renderer, row, col);
				JComponent jcomp = (JComponent) comp;
				comp.setBackground(new Color(0,0,0));
				jcomp.setBorder(new MatteBorder(0, 1, 1, 0, new Color(80,80,80)));
				return comp;
			}
		};
		teamScrollPane = new javax.swing.JScrollPane(teamTable);
		teamTable.setFillsViewportHeight(true);
		initColumnSizes(teamTable);
		setBuildSetsColumn(teamTable, teamTable.getColumnModel().getColumn(0));
		teamTable.setFont(new java.awt.Font("Verdana", 0, 11));
		centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
		teamTable.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
		teamTable.setDoubleBuffered(true);
		teamTable.setRowHeight(38);
		for (int i = 0; i < 9; i++){
			teamTable.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
			teamTable.getColumnModel().getColumn(i).setMaxWidth(150);
		}
		add(teamScrollPane);
		teamTextArea.setColumns(20);
		teamTextArea.setFont(new java.awt.Font("Verdana", 0, 12));
		teamTextArea.setLineWrap(true);
		teamTextArea.setRows(5);
		teamTextScrollPane.setViewportView(teamTextArea);
		pokemonTable.setFont(new java.awt.Font("Verdana", 0, 14));
		pokemonTable.setModel(new javax.swing.table.DefaultTableModel(PokemonGenerator.getSortedPokemonList("num"), new String[] { "GEN", "#", "SPECIES", "TYPE", "TIER", "HP", "ATK", "DEF", "SPA", "SPD", "SPE", "TOTAL BASE", "HEIGHT", "WEIGHT" }));
		pokemonTable.setEnabled(false);
		for (int i = 0; i < 14; i++)
			pokemonTable.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		pokemonScrollPane.setViewportView(pokemonTable);
		resizeColumnWidth(pokemonTable);
		teamTextArea.setEditable(false);
		teamTextScrollPane.setViewportView(teamTextArea);
		pokemonTable.setRowHeight(26);
		pokemonScrollPane.setViewportView(pokemonTable);
		teamResultsLabel.setFont(new java.awt.Font("Verdana", 0, 11));
		teamResultsLabel.setText("Team results below. #" + teams.size() + " teams generated. Starts at team one and clicking down goes to the next best team. Team #" + (index+1) + ".");sortNumButton.setFont(new java.awt.Font("Verdana", 0, 12));
		sortNumButton.setText("#");
		sortNumButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
		sortNumButton.setMaximumSize(new java.awt.Dimension(40, 30));
		sortNumButton.setMinimumSize(null);
		sortNumButton.setPreferredSize(new java.awt.Dimension(70, 35));
		sortNumButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sortNumButtonActionPerformed(evt);
			}
		});
		sortTypeButton.setFont(new java.awt.Font("Verdana", 0, 12));
		sortTypeButton.setText("Type");
		sortTypeButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
		sortTypeButton.setMaximumSize(new java.awt.Dimension(40, 30));
		sortTypeButton.setMinimumSize(null);
		sortTypeButton.setPreferredSize(new java.awt.Dimension(70, 35));
		sortTypeButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sortTypeButtonActionPerformed(evt);
			}
		});
		sortSpeciesButton.setFont(new java.awt.Font("Verdana", 0, 12));
		sortSpeciesButton.setText("Species");
		sortSpeciesButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
		sortSpeciesButton.setMaximumSize(new java.awt.Dimension(40, 30));
		sortSpeciesButton.setMinimumSize(null);
		sortSpeciesButton.setPreferredSize(new java.awt.Dimension(70, 35));
		sortSpeciesButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sortSpeciesButtonActionPerformed(evt);
			}
		});
		sortHpButton.setFont(new java.awt.Font("Verdana", 0, 12));
		sortHpButton.setText("HP");
		sortHpButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
		sortHpButton.setMaximumSize(new java.awt.Dimension(40, 30));
		sortHpButton.setMinimumSize(null);
		sortHpButton.setPreferredSize(new java.awt.Dimension(70, 35));
		sortHpButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sortHpButtonActionPerformed(evt);
			}
		});
		sortAtkButton.setFont(new java.awt.Font("Verdana", 0, 12));
		sortAtkButton.setText("Atk");
		sortAtkButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
		sortAtkButton.setMaximumSize(new java.awt.Dimension(35, 30));
		sortAtkButton.setPreferredSize(new java.awt.Dimension(70, 35));
		sortAtkButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sortAtkButtonActionPerformed(evt);
			}
		});
		sortTierButton.setFont(new java.awt.Font("Verdana", 0, 12));
		sortTierButton.setText("Tier");
		sortTierButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
		sortTierButton.setMaximumSize(new java.awt.Dimension(40, 30));
		sortTierButton.setMinimumSize(null);
		sortTierButton.setPreferredSize(new java.awt.Dimension(70, 35));
		sortTierButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sortTierButtonActionPerformed(evt);
			}
		});
		sortSpaButton.setFont(new java.awt.Font("Verdana", 0, 12));
		sortSpaButton.setText("SPA");
		sortSpaButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
		sortSpaButton.setMaximumSize(new java.awt.Dimension(40, 30));
		sortSpaButton.setMinimumSize(null);
		sortSpaButton.setPreferredSize(new java.awt.Dimension(70, 35));
		sortSpaButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sortSpaButtonActionPerformed(evt);
			}
		});
		sortDefButton.setFont(new java.awt.Font("Verdana", 0, 12));
		sortDefButton.setText("DEF");
		sortDefButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
		sortDefButton.setMaximumSize(new java.awt.Dimension(40, 30));
		sortDefButton.setMinimumSize(null);
		sortDefButton.setPreferredSize(new java.awt.Dimension(70, 35));
		sortDefButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sortDefButtonActionPerformed(evt);
			}
		});
		sortSpdButton.setFont(new java.awt.Font("Verdana", 0, 12));
		sortSpdButton.setText("SPD");
		sortSpdButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
		sortSpdButton.setMaximumSize(new java.awt.Dimension(40, 30));
		sortSpdButton.setMinimumSize(null);
		sortSpdButton.setPreferredSize(new java.awt.Dimension(70, 35));
		sortSpdButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sortSpdButtonActionPerformed(evt);
			}
		});
		sortSpeButton.setFont(new java.awt.Font("Verdana", 0, 12));
		sortSpeButton.setText("SPE");
		sortSpeButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
		sortSpeButton.setMaximumSize(new java.awt.Dimension(40, 30));
		sortSpeButton.setMinimumSize(null);
		sortSpeButton.setPreferredSize(new java.awt.Dimension(70, 35));
		sortSpeButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sortSpeButtonActionPerformed(evt);
			}
		});
		sortTotalButton.setFont(new java.awt.Font("Verdana", 0, 12));
		sortTotalButton.setText("Total");
		sortTotalButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
		sortTotalButton.setMaximumSize(new java.awt.Dimension(40, 30));
		sortTotalButton.setMinimumSize(null);
		sortTotalButton.setPreferredSize(new java.awt.Dimension(70, 35));
		sortTotalButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sortTotalButtonActionPerformed(evt);
			}
		});
		sortHeightButton.setFont(new java.awt.Font("Verdana", 0, 12));
		sortHeightButton.setText("Height");
		sortHeightButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
		sortHeightButton.setMaximumSize(new java.awt.Dimension(40, 30));
		sortHeightButton.setMinimumSize(null);
		sortHeightButton.setPreferredSize(new java.awt.Dimension(70, 35));
		sortHeightButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sortHeightButtonActionPerformed(evt);
			}
		});
		sortWeightButton.setFont(new java.awt.Font("Verdana", 0, 12));
		sortWeightButton.setText("Weight");
		sortWeightButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
		sortWeightButton.setMaximumSize(new java.awt.Dimension(40, 30));
		sortWeightButton.setMinimumSize(null);
		sortWeightButton.setPreferredSize(new java.awt.Dimension(70, 35));
		sortWeightButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sortWeightButtonActionPerformed(evt);
			}
		});
		upButton.setFont(new java.awt.Font("Verdana", 0, 11));
		upButton.setText("Up");
		upButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				upButtonActionPerformed(evt);
			}
		});
		downButton.setFont(new java.awt.Font("Verdana", 0, 11));
		downButton.setText("Down");
		downButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				downButtonActionPerformed(evt);
			}
		});
		quitButton.setFont(new java.awt.Font("Verdana", 0, 11));
		quitButton.setText("Quit");
		quitButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				quitButtonActionPerformed(evt);
			}
		});
		
		musicButton.setFont(new java.awt.Font("Verdana", 0, 11)); //Gabe line 933-939
 		musicButton.setText("Music");
    		musicButton.addActionListener(new java.awt.event.ActionListener() {
      		  	public void actionPerformed(java.awt.event.ActionEvent evt) {
         		   musicButtonActionPerformed(evt);
    			}
		    });
		
		helpButton.setFont(new java.awt.Font("Verdana", 0, 11));
		helpButton.setText("Help");
		helpButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				helpButtonActionPerformed(evt);
			}
		});
		sortByLabel.setFont(new java.awt.Font("Verdana", 0, 12));
		sortByLabel.setText("Sort by:");
		helpButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				helpButtonActionPerformed(evt);
			}
		});
		copyTextButton.setFont(new java.awt.Font("Verdana", 0, 11));
		copyTextButton.setText("Copy Text");
		copyTextButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				copyTextButtonActionPerformed(evt);
			}
		});
		updateFilesButton.setFont(new java.awt.Font("Verdana", 0, 11));
		updateFilesButton.setText("Update Files");
		updateFilesButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				try {
					updateFilesButtonActionPerformed(evt);
				} catch (IOException | ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		});
		doNotIncludeLabel.setFont(new java.awt.Font("Verdana", 0, 11));
		doNotIncludeLabel.setText("Do not include Pok�mon from generations:");
		doNotIncludeLabel.setAlignmentY(0.0F);
		generateTeamButton.setFont(new java.awt.Font("Verdana", 0, 11));
		generateTeamButton.setText("Generate Team");
		generateTeamButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				generateTeamButtonActionPerformed(evt);
			}
		});
		formatComboBox.setFont(new java.awt.Font("Verdana", 0, 11));
		formatComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] {"AG", "Uber", "OU", "BL", "UU", "BL2", "RU", "BL3", "NU", "BL4", "PU", "NFE", "LC Uber", "LC" }));
		formatLabel.setFont(new java.awt.Font("Verdana", 0, 11));
		formatLabel.setText("Format");
		doNotIncludeLabelTable.setFont(new java.awt.Font("Verdana", 0, 11));
		doNotIncludeLabelTable.setText("Do not include Pok�mon from generations:");
		doNotIncludeLabelTable.setAlignmentY(0.0F);
		genTableSixCheckBox.setFont(new java.awt.Font("Verdana", 0, 11));
		genTableSixCheckBox.setText("6");
		genTableSixCheckBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				genTableSixActionPerformed(evt);
			}
		});
		genTableFiveCheckBox.setFont(new java.awt.Font("Verdana", 0, 11));
		genTableFiveCheckBox.setText("5");
		genTableFiveCheckBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				genTableFiveActionPerformed(evt);
			}
		});
		genTableFourCheckBox.setFont(new java.awt.Font("Verdana", 0, 11));
		genTableFourCheckBox.setText("4");
		genTableFourCheckBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				genTableFourActionPerformed(evt);
			}
		});
		genTableThreeCheckBox.setFont(new java.awt.Font("Verdana", 0, 11));
		genTableThreeCheckBox.setText("3");
		genTableThreeCheckBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				genTableThreeActionPerformed(evt);
			}
		});
		genTableTwoCheckBox.setFont(new java.awt.Font("Verdana", 0, 11));
		genTableTwoCheckBox.setText("2");
		genTableTwoCheckBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				genTableTwoActionPerformed(evt);
			}
		});
		genTableOneCheckBox.setFont(new java.awt.Font("Verdana", 0, 11));
		genTableOneCheckBox.setText("1");
		genTableOneCheckBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				genTableOneActionPerformed(evt);
			}
		});
		genSixCheckBox.setFont(new java.awt.Font("Verdana", 0, 11));
		genSixCheckBox.setText("6");
		genSixCheckBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				genSixCheckBoxActionPerformed(evt);
			}
		});
		genFiveCheckBox.setFont(new java.awt.Font("Verdana", 0, 11));
		genFiveCheckBox.setText("5");
		genFiveCheckBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				genFiveCheckBoxActionPerformed(evt);
			}
		});
		genFourCheckBox.setFont(new java.awt.Font("Verdana", 0, 11));
		genFourCheckBox.setText("4");
		genFourCheckBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				genFourCheckBoxActionPerformed(evt);
			}
		});
		genThreeCheckBox.setFont(new java.awt.Font("Verdana", 0, 11));
		genThreeCheckBox.setText("3");
		genThreeCheckBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				genThreeCheckBoxActionPerformed(evt);
			}
		});
		genTwoCheckBox.setFont(new java.awt.Font("Verdana", 0, 11));
		genTwoCheckBox.setText("2");
		genTwoCheckBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				genTwoCheckBoxActionPerformed(evt);
			}
		});
		genOneCheckBox.setFont(new java.awt.Font("Verdana", 0, 11));
		genOneCheckBox.setText("1");
		genOneCheckBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				genOneCheckBoxActionPerformed(evt);
			}
		});

		doNotIncludeLabel.setText("Do not include Pok�mon from generations:");
		
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(layout.createSequentialGroup().addGap(10, 10,10).addComponent(teamScrollPane)
									.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(
										layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
											.addComponent(downButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
												.addComponent(upButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
						.addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(pokemonScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 1342, Short.MAX_VALUE)
								.addGroup(layout.createSequentialGroup().addGap(8, 8, 8).addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(teamResultsLabel).addGap(7, 7, 7).addComponent(generateTeamButton))
									.addGroup(layout.createSequentialGroup().addComponent(doNotIncludeLabel).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(genOneCheckBox)
											.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(genTwoCheckBox).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(genThreeCheckBox)
											.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(genFourCheckBox).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(genFiveCheckBox)
											.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(genSixCheckBox).addGap(27, 27, 27).addComponent(formatLabel).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(formatComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
											.addGap(0, 0,Short.MAX_VALUE)))))
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
										.addComponent(quitButton).addComponent(teamTextScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE).addComponent(helpButton).addComponent(copyTextButton).addComponent(updateFilesButton).addComponent(musicButton))
										.addContainerGap())
						.addGroup(layout.createSequentialGroup().addGap(19, 19, 19)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(layout.createSequentialGroup().addComponent(sortByLabel).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(sortNumButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(sortSpeciesButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(sortTypeButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(sortTierButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(sortHpButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(sortDefButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(sortSpaButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(sortSpdButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(sortSpeButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(sortTotalButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(sortHeightButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(sortWeightButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGroup(layout.createSequentialGroup().addGap(6, 6, 6).addComponent(doNotIncludeLabelTable).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(genTableOneCheckBox)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(genTableTwoCheckBox).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(genTableThreeCheckBox)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(genTableFourCheckBox).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(genTableFiveCheckBox)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(genTableSixCheckBox)))
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup().addContainerGap()
										.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(sortByLabel).addComponent(sortNumButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(sortSpeciesButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(sortTypeButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(sortTierButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(sortHpButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(sortDefButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(sortSpaButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(sortSpdButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(sortSpeButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(sortTotalButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(sortHeightButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(sortWeightButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,javax.swing.GroupLayout.PREFERRED_SIZE)).addGap(16, 16,16)
										.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(genTableFiveCheckBox).addComponent(genTableSixCheckBox))
										.addComponent(genTableFourCheckBox).addComponent(genTableThreeCheckBox)
										.addComponent(genTableTwoCheckBox).addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(genTableOneCheckBox).addComponent(doNotIncludeLabelTable))).addGap(22, 22, 22)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
						.addGroup(layout.createSequentialGroup().addComponent(updateFilesButton)
		                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
		                        .addComponent(musicButton)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(helpButton).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(quitButton).addGap(15, 15, 15).addComponent(copyTextButton)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(teamTextScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 513, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGroup(layout.createSequentialGroup().addComponent(pokemonScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 455, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER).addComponent(genOneCheckBox).addComponent(genTwoCheckBox).addComponent(genThreeCheckBox).addComponent(genFourCheckBox).addComponent(genFiveCheckBox).addComponent(genSixCheckBox)
												.addComponent(doNotIncludeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
												.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(formatComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(formatLabel)))
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(teamResultsLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE).addComponent(generateTeamButton)).addGap(8, 8, 8)
										.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
												.addGroup(layout.createSequentialGroup().addComponent(upButton).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(downButton).addGap(209, 209, 209))
												.addComponent(teamScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))))
										.addContainerGap()));
		pack();
	}

	private void genTableOneActionPerformed(java.awt.event.ActionEvent evt) {
		genTableOne = !genTableOne;
	}

	private void genTableTwoActionPerformed(java.awt.event.ActionEvent evt) {
		genTableTwo = !genTableTwo;
	}

	private void genTableThreeActionPerformed(java.awt.event.ActionEvent evt) {
		genTableThree = !genTableThree;
	}

	private void genTableFourActionPerformed(java.awt.event.ActionEvent evt) {
		genTableFour = !genTableFour;
	}

	private void genTableFiveActionPerformed(java.awt.event.ActionEvent evt) {
		genTableFive = !genTableFive;
	}

	private void genTableSixActionPerformed(java.awt.event.ActionEvent evt) {
		genTableSix = !genTableSix;
	}

	private void genOneCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {
		genOne = !genOne;
	}

	private void genTwoCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {
		genTwo = !genTwo;
	}

	private void genThreeCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {
		genThree = !genThree;
	}

	private void genFourCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {
		genFour = !genFour;
	}

	private void genFiveCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {
		genFive = !genFive;
	}

	private void genSixCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {
		genSix = !genSix;
	}

	private void quitButtonActionPerformed(java.awt.event.ActionEvent evt) {
		System.exit(0);
	}
	
	private void musicButtonActionPerformed(java.awt.event.ActionEvent evt) {
		if (music == false) {
			musicButton.setText("Music - ON");
			try {
				InputStream test = new FileInputStream(PokemonGenerator.directory + "/theme song.wav");
				BGM = new AudioStream(test);
			} catch (Exception e) {
				System.out.println(e);
			}
			AudioPlayer.player.start(BGM);
			MGP.start(loop);
			music = true;
		} else {
			musicButton.setText("Music");
			AudioPlayer.player.stop(BGM);
			MGP.stop(loop);
			music = false;
		}
	};
//		 AudioData MD; // This should work but no sound plays
//		 ContinuousAudioDataStream loop = null;
//		if (music == false) {
//			musicButton.setText("Music - ON");
//			try{
//				BGM = new AudioStream(new FileInputStream(PokemonGenerator.directory + "/theme song.wav"));
//				MD = BGM.getData();
//				loop = new ContinuousAudioDataStream(MD);
//				MGP.start(loop);
//			} catch (Exception e) {}
//			music = true;
//		} else {
//			musicButton.setText("Music");
//			AudioPlayer.player.stop(BGM);
//			MGP.stop(loop);
//			music = false;
//		}
//	};

	private void helpButtonActionPerformed(java.awt.event.ActionEvent evt) {}

	private void upButtonActionPerformed(java.awt.event.ActionEvent evt) {
		if (index == 0) return;
		index--;
		updateTeamTable();
		teamResultsLabel.setText("Team results below. #" + teams.size() + " teams generated. Starts at team one and clicking down goes to the next best team. Team #" + (index + 1) + ".");
	}

	private void downButtonActionPerformed(java.awt.event.ActionEvent evt) {
		if (index == teams.size() - 1) return;
		index++;
		updateTeamTable();
		teamResultsLabel.setText("Team results below. #" + teams.size() + " teams generated. Starts at team one and clicking down goes to the next best team. Team #" + (index + 1) + ".");
	}

	public static void sortNumButtonActionPerformed(java.awt.event.ActionEvent evt) {
		pokemonTable.setModel(new javax.swing.table.DefaultTableModel(PokemonGenerator.getSortedPokemonList("num"), new String[] { "GEN", "#", "SPECIES", "TYPE", "TIER", "HP", "ATK", "DEF", "SPA", "SPD", "SPE", "TOTAL BASE", "HEIGHT", "WEIGHT" }));
		for (int i = 0; i < 14; i++)
			pokemonTable.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		resizeColumnWidth(pokemonTable);
		pokemonScrollPane.setViewportView(pokemonTable);
		pokemonTable.repaint();
	}

	private void sortSpeciesButtonActionPerformed(java.awt.event.ActionEvent evt) {
		pokemonTable.setModel(new javax.swing.table.DefaultTableModel(PokemonGenerator.getSortedPokemonList("species"), new String[] { "GEN", "#", "SPECIES", "TYPE", "TIER", "HP", "ATK", "DEF", "SPA", "SPD", "SPE", "TOTAL BASE", "HEIGHT", "WEIGHT" }));
		for (int i = 0; i < 14; i++)
			pokemonTable.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		resizeColumnWidth(pokemonTable);
		pokemonScrollPane.setViewportView(pokemonTable);
		pokemonTable.repaint();
	}

	private void sortTypeButtonActionPerformed(java.awt.event.ActionEvent evt) {
		pokemonTable.setModel(new javax.swing.table.DefaultTableModel(PokemonGenerator.getSortedPokemonList("type"), new String[] { "GEN", "#", "SPECIES", "TYPE", "TIER", "HP", "ATK", "DEF", "SPA", "SPD", "SPE", "TOTAL BASE", "HEIGHT", "WEIGHT" }));
		for (int i = 0; i < 14; i++)
			pokemonTable.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		resizeColumnWidth(pokemonTable);
		pokemonScrollPane.setViewportView(pokemonTable);
		pokemonTable.repaint();
	}

	private void sortTierButtonActionPerformed(java.awt.event.ActionEvent evt) {
		pokemonTable.setModel(new javax.swing.table.DefaultTableModel(PokemonGenerator.getSortedPokemonList("tier"), new String[] { "GEN", "#", "SPECIES", "TYPE", "TIER", "HP", "ATK", "DEF", "SPA", "SPD", "SPE", "TOTAL BASE", "HEIGHT", "WEIGHT" }));
		for (int i = 0; i < 14; i++)
			pokemonTable.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		resizeColumnWidth(pokemonTable);
		pokemonScrollPane.setViewportView(pokemonTable);
		pokemonTable.repaint();
	}

	private void sortHpButtonActionPerformed(java.awt.event.ActionEvent evt) {
		pokemonTable.setModel(new javax.swing.table.DefaultTableModel(PokemonGenerator.getSortedPokemonList("hp"), new String[] { "GEN", "#", "SPECIES", "TYPE", "TIER", "HP", "ATK", "DEF", "SPA", "SPD", "SPE", "TOTAL BASE", "HEIGHT", "WEIGHT" }));
		for (int i = 0; i < 14; i++)
			pokemonTable.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		resizeColumnWidth(pokemonTable);
		pokemonScrollPane.setViewportView(pokemonTable);
		pokemonTable.repaint();
	}

	private void sortAtkButtonActionPerformed(java.awt.event.ActionEvent evt) {
		pokemonTable.setModel(new javax.swing.table.DefaultTableModel(PokemonGenerator.getSortedPokemonList("atk"), new String[] { "GEN", "#", "SPECIES", "TYPE", "TIER", "HP", "ATK", "DEF", "SPA", "SPD", "SPE", "TOTAL BASE", "HEIGHT", "WEIGHT" }));
		for (int i = 0; i < 14; i++)
			pokemonTable.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		resizeColumnWidth(pokemonTable);
		pokemonScrollPane.setViewportView(pokemonTable);
		pokemonTable.repaint();
	}

	private void sortDefButtonActionPerformed(java.awt.event.ActionEvent evt) {
		pokemonTable.setModel(new javax.swing.table.DefaultTableModel(PokemonGenerator.getSortedPokemonList("def"), new String[] { "GEN", "#", "SPECIES", "TYPE", "TIER", "HP", "ATK", "DEF", "SPA", "SPD", "SPE", "TOTAL BASE", "HEIGHT", "WEIGHT" }));
		for (int i = 0; i < 14; i++)
			pokemonTable.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		resizeColumnWidth(pokemonTable);
		pokemonScrollPane.setViewportView(pokemonTable);
		pokemonTable.repaint();
	}

	private void sortSpaButtonActionPerformed(java.awt.event.ActionEvent evt) {
		pokemonTable.setModel(new javax.swing.table.DefaultTableModel(PokemonGenerator.getSortedPokemonList("spa"), new String[] { "GEN", "#", "SPECIES", "TYPE", "TIER", "HP", "ATK", "DEF", "SPA", "SPD", "SPE", "TOTAL BASE", "HEIGHT", "WEIGHT" }));
		for (int i = 0; i < 14; i++)
			pokemonTable.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		resizeColumnWidth(pokemonTable);
		pokemonScrollPane.setViewportView(pokemonTable);
		pokemonTable.repaint();
	}

	private void sortSpdButtonActionPerformed(java.awt.event.ActionEvent evt) {
		pokemonTable.setModel(new javax.swing.table.DefaultTableModel(PokemonGenerator.getSortedPokemonList("spd"), new String[] { "GEN", "#", "SPECIES", "TYPE", "TIER", "HP", "ATK", "DEF", "SPA", "SPD", "SPE", "TOTAL BASE", "HEIGHT", "WEIGHT" }));
		for (int i = 0; i < 14; i++)
			pokemonTable.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		resizeColumnWidth(pokemonTable);
		pokemonScrollPane.setViewportView(pokemonTable);
		pokemonTable.repaint();
	}

	private void sortSpeButtonActionPerformed(java.awt.event.ActionEvent evt) {
		pokemonTable.setModel(new javax.swing.table.DefaultTableModel(PokemonGenerator.getSortedPokemonList("spe"), new String[] { "GEN", "#", "SPECIES", "TYPE", "TIER", "HP", "ATK", "DEF", "SPA", "SPD", "SPE", "TOTAL BASE", "HEIGHT", "WEIGHT" }));
		for (int i = 0; i < 14; i++)
			pokemonTable.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		resizeColumnWidth(pokemonTable);
		pokemonScrollPane.setViewportView(pokemonTable);
		pokemonTable.repaint();
	}

	private void sortTotalButtonActionPerformed(java.awt.event.ActionEvent evt) {
		pokemonTable.setModel(new javax.swing.table.DefaultTableModel(PokemonGenerator.getSortedPokemonList("totalbase"), new String[] { "GEN", "#", "SPECIES", "TYPE", "TIER", "HP", "ATK", "DEF", "SPA", "SPD", "SPE", "TOTAL BASE", "HEIGHT", "WEIGHT" }));
		for (int i = 0; i < 14; i++)
			pokemonTable.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		resizeColumnWidth(pokemonTable);
		pokemonScrollPane.setViewportView(pokemonTable);
		pokemonTable.repaint();
	}

	private void sortHeightButtonActionPerformed(java.awt.event.ActionEvent evt) {
		pokemonTable.setModel(new javax.swing.table.DefaultTableModel(PokemonGenerator.getSortedPokemonList("height"), new String[] { "GEN", "#", "SPECIES", "TYPE", "TIER", "HP", "ATK", "DEF", "SPA", "SPD", "SPE", "TOTAL BASE", "HEIGHT", "WEIGHT" }));
		for (int i = 0; i < 14; i++)
			pokemonTable.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		resizeColumnWidth(pokemonTable);
		pokemonScrollPane.setViewportView(pokemonTable);
		pokemonTable.repaint();
	}

	private void sortWeightButtonActionPerformed(java.awt.event.ActionEvent evt) {
		pokemonTable.setModel(new javax.swing.table.DefaultTableModel(PokemonGenerator.getSortedPokemonList("weight"), new String[] { "GEN", "#", "SPECIES", "TYPE", "TIER", "HP", "ATK", "DEF", "SPA", "SPD", "SPE", "TOTAL BASE", "HEIGHT", "WEIGHT" }));
		for (int i = 0; i < 14; i++)
			pokemonTable.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		resizeColumnWidth(pokemonTable);
		pokemonScrollPane.setViewportView(pokemonTable);
		pokemonTable.repaint();
	}

	private void copyTextButtonActionPerformed(java.awt.event.ActionEvent evt) {
		StringSelection stringSelection = new StringSelection(teamTextArea.getText());
		Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
		clpbrd.setContents(stringSelection, null);
	}

	private void updateTextArea() { // Updates text area
		String value = "";
		teamTextArea.setText(value);
		for (int i = 0; i < 6; i++) {
			try{if(teamTable.getValueAt(i, 1).equals("")) break; } catch (Exception e) {}
			try{value += teamTable.getValueAt(i, 1) + " @ ";} catch (Exception e) {} // Species
			try{value += teamTable.getValueAt(i, 8) + "\n";} catch (Exception e) {} // item
			try{value += "Ability: " + teamTable.getValueAt(i, 3) + "\n";} catch (Exception e) {} // Ability
			try{value += teams.get(index).get(i).BuildSets.get(4).get(0) + "\n";} catch (Exception e) {}
			try{if (teams.get(index).get(i).BuildSets.get(5).get(0).contains(",")) {
				String[] multipleElements = teams.get(index).get(i).BuildSets.get(5).get(0).split(",");
				value += multipleElements[0] + " Nature";
			} else try{ value += teams.get(index).get(i).BuildSets.get(5).get(0) + " Nature"; } catch (Exception e) {}} catch (Exception e) {}
			try{value += "\n- " + teamTable.getValueAt(i, 4) + "\n";} catch (Exception e) {} // Moves
			try{value += "- " + teamTable.getValueAt(i, 5) + "\n"; } catch (Exception e) {}// Moves
			try{value += "- " + teamTable.getValueAt(i, 6) + "\n"; } catch (Exception e) {}// Moves
			try{value += "- " + teamTable.getValueAt(i, 7) + "\n\n"; } catch (Exception e) {}// Moves
		}
		
		// All this code just removes the html tags so the text area works properly
		value = value.replaceAll("<html>|<strong>|</font>|</strong>|</html>", "");
		String[] values = value.split("\n");
		String newValue = "";
		for(String v : values){
			int vWithMultiple = v.length();
			int vWithoutMultiple = v.replace("color", "").length();
			if(vWithMultiple - vWithoutMultiple >= 2) {
			    String[] whatWeWant = v.split("<font|>");
			    for(String w : whatWeWant){
			    	if(!w.contains("color")){
			    		newValue += w;
			    	}
			    }
			    newValue += "\n";
			}
			else newValue += v.substring(v.lastIndexOf(">")+1) + "\n";
		}
		teamTextArea.setText(newValue);
	}

	private void updateTeamTable() { // Updates teams table
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 9; j++) {
			teamTable.setValueAt("", i, j);
			}
		}
		for (int i = 0; i < 6; i++) {
//			try{teamTable.setValueAt(teams.get(index).get(i).BuildSets.get(0).get(0), i, 0); } catch (Exception e) {} // Build Name
			try{teamTable.setValueAt(teams.get(index).get(i).getSPECIES(), i, 1); } catch (Exception e) { } // Pokemon name
			try{teamTable.setValueAt(teams.get(index).get(i).getTYPES(), i, 2); } catch (Exception e) {} // Type
			try {if(teams.get(index).get(i).BuildSets.get(2).get(0).contains(",")){
				String[] multipleElements = teams.get(index).get(i).BuildSets.get(2).get(0).split(",");
				teamTable.setValueAt(multipleElements[0], i, 3); } // Abilities
				else try{teamTable.setValueAt(teams.get(index).get(i).BuildSets.get(2).get(0), i, 3); } catch (Exception e) {}} catch (Exception e) {}
			
			try{if(teams.get(index).get(i).BuildSets.get(3).get(0).contains(",")){
			String[] multipleElements = teams.get(index).get(i).BuildSets.get(3).get(0).split(","); // Moves
				for (int j = 0; j < 4; j++)
					try{teamTable.setValueAt(multipleElements[j], i, (4 + j));} catch (Exception e) {}} // - Moves}
			else try{teamTable.setValueAt(teams.get(index).get(i).BuildSets.get(3).get(0), i, 4); } catch (Exception e) {}} catch (Exception e) {}
				try{if (teams.get(index).get(i).BuildSets.get(1).get(0).contains(",")) {
					String[] multipleElements = teams.get(index).get(i).BuildSets.get(1).get(0).split(","); // Items
					teamTable.setValueAt(multipleElements[0], i, 8); // - Items -
				} else teamTable.setValueAt(teams.get(index).get(i).BuildSets.get(1).get(0), i, 8);  } catch (Exception e) {} // - Items
		}
		for (int i = 0; i < 6; i++) {
			String type = teams.get(index).get(i).getTYPES();
			Color color = getColorFromTypeCell(type);
//			try{teamTable.setValueAt(updateTextWithColor(teams.get(index).get(i).BuildSets.get(0).get(0), color), i, 0); } catch (Exception e) {} // Build name with color
			try{teamTable.setValueAt(updateTextWithColor(teams.get(index).get(i).getSPECIES(), color), i, 1); } catch (Exception e) { } // Pokemon name with color
			try{teamTable.setValueAt(updateTextWithColor(teams.get(index).get(i).getTYPES(),color), i, 2); } catch (Exception e) {} // Type with color
			
			
			try{if(teams.get(index).get(i).BuildSets.get(2).get(0).contains(",")){ // Abilities with color
				String[] multipleElements = teams.get(index).get(i).BuildSets.get(2).get(0).split(","); // Abilities with color
						try{teamTable.setValueAt(updateTextWithColor(multipleElements[0], color), i, 3); // Abilities with color
						teamTable.setValueAt(updateTextWithColor(multipleElements[0], color), i, 3);} catch (Exception e) {}} // Abilities with color
				else try{teamTable.setValueAt(updateTextWithColor(teams.get(index).get(i).BuildSets.get(2).get(0), color), i, 3); } catch (Exception e) {}} catch (Exception e) {}

			try{if(teams.get(index).get(i).BuildSets.get(3).get(0).contains(",")){
				String[] multipleElements = teams.get(index).get(i).BuildSets.get(3).get(0).split(","); // Moves
					for (int j = 0; j < 4; j++)
						try{teamTable.setValueAt(updateTextWithColor(multipleElements[j], color), i, (4 + j)); // - Moves
						teamTable.setValueAt(updateTextWithColor(multipleElements[j], color), i, (4 + j));} catch (Exception e) {}} // - Moves}
				else try{teamTable.setValueAt(updateTextWithColor(teams.get(index).get(i).BuildSets.get(3).get(0), color), i, 4); } catch (Exception e) {}} catch (Exception e) {}
					try{if (teams.get(index).get(i).BuildSets.get(1).get(0).contains(",")) {
						String[] multipleElements = teams.get(index).get(i).BuildSets.get(1).get(0).split(","); // Items
						teamTable.setValueAt(updateTextWithColor(multipleElements[0], color), i, 8); // - Items -
					} else teamTable.setValueAt(updateTextWithColor(teams.get(index).get(i).BuildSets.get(1).get(0),color), i, 8);  } catch (Exception e) {} // - Items
		}
		updateTextArea();
		changeBuildSet(teamTable);
		resizeColumnWidth(teamTable);
	}

	private void checkPokemon() { // AG, Uber, OU, BL, UU, BL2, RU, BL3, NU, BL4, PU, NFE, LC Uber, LC
		int selectedFormatNumber = 0; int counter = 0;
		String formatSelected = (String) formatComboBox.getSelectedItem();
		ArrayList<String> list = new ArrayList<String>();
		for(String s : PokemonGenerator.formats){
			if(formatSelected.equals(s)){
				selectedFormatNumber = counter;
				break;
			}
			counter++;
		}
		for(int a = selectedFormatNumber; a < PokemonGenerator.formats.length; a++) list.add(PokemonGenerator.formats[a]);
		list.add("Illegal");
		list.add("Unreleased");
		if (genOne == false) list.add("1");
		if (genTwo == false) list.add("2");
		if (genThree == false) list.add("3");
		if (genFour == false) list.add("4");
		if (genFive == false) list.add("5");
		if (genSix == false) list.add("6");
		while (true) {
			for (int i = 0; i < list.size();) {
				if(PokemonGenerator.PokemonList.size() == 0) return;
				if (Integer.toString(PokemonGenerator.PokemonList.get(0).getGEN()).equals(list.get(i)) || PokemonGenerator.PokemonList.get(0).getSPECIES().contains(list.get(i)) || !(list.contains(PokemonGenerator.PokemonList.get(0).getTIER()))) {
					PokemonGenerator.PokemonList.remove(0);
					i = 0;
				} else i++;
			}
			break;
		}
		System.out.println(PokemonGenerator.PokemonList.get(0).getSPECIES() + " " + PokemonGenerator.PokemonList.get(0).getTIER());
	}
	
    private void pokemonTeamGenerate(){
    	index = 0;
    	int teamNum = 0;
    	PokemonGenerator.getSortedPokemonList("totalbase");
		selectedList.clear(); // Reset the ArrayList of pokemon selected by the generated teams
    	teams.clear(); // Reset ArrayList of teams back to zero or else it would keep stacking more slots every time
		for(Pokemon p : PokemonGenerator.noBuildList) selectedList.add(p);		
			for (int j = 0; j < selectedList.size();j++) {
				for(int i = 0; i < PokemonGenerator.PokemonList.size();i++) {
				if (PokemonGenerator.PokemonList.get(i).getSPECIES().equals(selectedList.get(j).getSPECIES())) {
					PokemonGenerator.PokemonList.remove(i); }
			}
		}
    	whileLoop:
    	while(true){ // Generate as many teams as possible
			ArrayList<Pokemon> newTeam = new ArrayList<Pokemon>(); // Adds a new team to the nested teams arraylist
			teams.add(newTeam);
			for (int i = 0; i < 6; i++) { // Assigns six pokemon per team
				checkPokemon();
				if (PokemonGenerator.PokemonList.size() == 0) break whileLoop; // If we generated teams with all the possible pokemon, break
				selectedList.add(PokemonGenerator.PokemonList.get(0));
				teams.get(teamNum).add(PokemonGenerator.PokemonList.remove(0));
				try {
				} catch (Exception e) {}
			}
			if (PokemonGenerator.PokemonList.size() == 0) break;
			teamNum++;
    	}
        resizeColumnWidth(teamTable);
		teamResultsLabel.setText("Team results below. #" + teams.size() + " teams generated. Starts at team one and clicking down goes to the next best team. Team #" + (index+1) + ".");
        updateTextArea();
    }
	private void generateTeamButtonActionPerformed(java.awt.event.ActionEvent evt) {
		if (genOne == false && genTwo == false && genThree == false && genFour == false && genFive == false && genSix == false) return;
		pokemonTeamGenerate();
		updateTeamTable();
	}

	// Need to fix this with http://stackoverflow.com/questions/4159802/how-can-i-restart-a-java-application
	private void updateFilesButtonActionPerformed(java.awt.event.ActionEvent evt) throws IOException, ClassNotFoundException {
		Files.deleteIfExists(FileSystems.getDefault().getPath(PokemonGenerator.directory, "Data typechart.txt"));
//		Files.deleteIfExists(FileSystems.getDefault().getPath(PokemonGenerator.directory, "Data pokedex.txt"));
		Files.deleteIfExists(FileSystems.getDefault().getPath(PokemonGenerator.directory, "Data formats-data.txt"));
		Files.deleteIfExists(FileSystems.getDefault().getPath(PokemonGenerator.directory, "PokemonList.bin"));
		Files.deleteIfExists(FileSystems.getDefault().getPath(PokemonGenerator.directory, "TypeChart.bin"));
		Files.deleteIfExists(FileSystems.getDefault().getPath(PokemonGenerator.directory, "NoBuildList.bin"));
		PokemonGenerator.deleteEverything();
//		dispose();
//		PokemonGenerator.main(null);
//		String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
//		File currentJar = null;
//		try { currentJar = new File(PokemonGenerator.class.getProtectionDomain().getCodeSource().getLocation().toURI());
//		} catch (URISyntaxException e) { e.printStackTrace(); }
//		/* is it a jar file? */
//		if (!currentJar.getName().endsWith(".jar")) return;
//		/* Build command: java -jar application.jar */
//		final ArrayList<String> command = new ArrayList<String>();
//		command.add(javaBin);
//		command.add("-jar");
//		command.add(currentJar.getPath());
//		final ProcessBuilder builder = new ProcessBuilder(command);
//		builder.start();
		System.exit(0);
	}

	public static void main(String args[]) {
		try { UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HifiLookAndFeel");  } catch (Exception e) { }
	}
	// Variables declaration - do not modify     
	private boolean genOne = true;
	private boolean genTwo = true;
	private boolean genThree = true;
	private boolean genFour = true;
	private boolean genFive = true;
	private boolean genSix = true;
	private boolean genTableOne = true;
	private boolean genTableTwo = true;
	private boolean genTableThree = true;
	private boolean genTableFour = true;
	private boolean genTableFive = true;
	private boolean genTableSix = true;
	private int index = 0;
	private static DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
	private javax.swing.JButton copyTextButton;
	private javax.swing.JLabel doNotIncludeLabel;
	private javax.swing.JButton musicButton;
	private javax.swing.JButton downButton;
	private javax.swing.JCheckBox genFiveCheckBox;
	private javax.swing.JCheckBox genTableFiveCheckBox;
	private javax.swing.JCheckBox genFourCheckBox;
	private javax.swing.JCheckBox genTableFourCheckBox;
	private javax.swing.JCheckBox genOneCheckBox;
	private javax.swing.JCheckBox genTableOneCheckBox;
	private javax.swing.JCheckBox genSixCheckBox;
	private javax.swing.JCheckBox genTableSixCheckBox;
	private javax.swing.JCheckBox genThreeCheckBox;
	private javax.swing.JCheckBox genTableThreeCheckBox;
	private javax.swing.JCheckBox genTwoCheckBox;
	private javax.swing.JCheckBox genTableTwoCheckBox;
	private javax.swing.JButton generateTeamButton;
	private javax.swing.JButton helpButton;
	private javax.swing.JComboBox<String> formatComboBox;
	private javax.swing.JLabel formatLabel;
	private javax.swing.JLabel doNotIncludeLabelTable;
	private static javax.swing.JScrollPane pokemonScrollPane;
	private static javax.swing.JTable pokemonTable;
	private javax.swing.JButton quitButton;
	private javax.swing.JLabel sortByLabel;
	private javax.swing.JButton sortDefButton;
	private javax.swing.JButton sortHeightButton;
	private javax.swing.JButton sortHpButton;
	private javax.swing.JButton sortAtkButton;
	private javax.swing.JButton sortNumButton;
	private javax.swing.JButton sortSpaButton;
	private javax.swing.JButton sortSpdButton;
	private javax.swing.JButton sortSpeButton;
	private javax.swing.JButton sortSpeciesButton;
	private javax.swing.JButton sortTierButton;
	private javax.swing.JButton sortTotalButton;
	private javax.swing.JButton sortTypeButton;
	private javax.swing.JButton sortWeightButton;
	private javax.swing.JLabel teamResultsLabel;
	private javax.swing.JScrollPane teamScrollPane;
	private javax.swing.JTable teamTable;
	private javax.swing.JTextArea teamTextArea;
	private javax.swing.JScrollPane teamTextScrollPane;
	private javax.swing.JButton upButton;
	private javax.swing.JButton updateFilesButton;
}


